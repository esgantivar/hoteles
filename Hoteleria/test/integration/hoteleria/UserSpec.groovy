package hoteleria



import hoteleria.User;

import java.util.Map;

import spock.lang.*

/**
 *
 */
class UserSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test something"() {
    }
	
	void 'insertar usuarios con el mismo email'() {
		given:
		User user = createUser(email:"pepe@pepe.com")
		User user2 = createUser(email:"pepe@pepe.com")
		
		when:
		boolean validated = user.validate()
		User saved = user.save()
		
		then:
		validated
		saved
		
		when:
		boolean validated2 = user2.validate()
		User saved2 = user2.save()
		
		then:
		!validated2
		!saved2
	}
	
	private User createUser(Map data) {
		def user = new User(userName:"pepe", password:"abc123",
			 email:"pepe@pepe.com", address:"asd 123", telephone:"3211568")
		user.properties = data
		user
	}
}
