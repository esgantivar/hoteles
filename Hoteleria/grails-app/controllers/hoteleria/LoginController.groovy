package hoteleria

class LoginController {

	def index() {
	}


	def login = {

		def user = User.findByEmailAndPassword(params['email'],params['password'])

//		if(user && user.role == 'Usuario'){
//			flash.message = "Hola!"
//			redirect (controller:'User',action: 'index')
//		}else{
//			if(user && user.role == 'Administrador'){
//				flash.message = "Hola!"
//				redirect (controller:'Admin',action: 'index')
//			}
//			else{
//				flash.message = "El nombre de usuario y/o contrase\u00f1a no existen"
//			}
//		}
		
		if(user != null){
			session['user']= user.userName
			session['rol']= user.rol
			session['userid']= user.id
			redirect (controller:'hotel',action: 'index')
		}else{
			flash.message = "El nombre de usuario y/o contrase\u00f1a no existen"
			redirect (controller:'Login',action: 'index')
		}
	}

	def logout = {
		session.invalidate()
		redirect(action: 'index')
	}
}

