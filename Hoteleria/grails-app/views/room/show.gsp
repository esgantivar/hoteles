
<%@ page import="hoteleria.Room"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="hoteleriaLayout">
<g:set var="entityName"
	value="${message(code: 'room.label', default: 'Room')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<section id="contact-page">
		<div class="container">

			<div class="left">
				<br /> <br />
				<h2>Datos de la Habitacion</h2>
				<g:if test="${flash.message}">
					<div class="message" role="status">
						${flash.message}
					</div>
				</g:if>
				<ol class="property-list room">

					<g:if test="${roomInstance?.numerOfPeople}">
						<li class="fieldcontain"><span id="numerOfPeople-label"
							class="property-label"><g:message
									code="room.numerOfPeople.label" default="Numer Of People" /></span> <span
							class="property-value" aria-labelledby="numerOfPeople-label"><g:fieldValue
									bean="${roomInstance}" field="numerOfPeople" /></span></li>
					</g:if>

					<g:if test="${roomInstance?.priceRoom}">
						<li class="fieldcontain"><span id="priceRoom-label"
							class="property-label"><g:message
									code="room.priceRoom.label" default="Price Room" /></span> <span
							class="property-value" aria-labelledby="priceRoom-label"><g:fieldValue
									bean="${roomInstance}" field="priceRoom" /></span></li>
					</g:if>

					<g:if test="${roomInstance?.startDateRoom}">
						<li class="fieldcontain"><span id="startDateRoom-label"
							class="property-label"><g:message
									code="room.startDateRoom.label" default="Start Date Room" /></span> <span
							class="property-value" aria-labelledby="startDateRoom-label"><g:formatDate
									date="${roomInstance?.startDateRoom}" /></span></li>
					</g:if>

					<g:if test="${roomInstance?.endDateRoom}">
						<li class="fieldcontain"><span id="endDateRoom-label"
							class="property-label"><g:message
									code="room.endDateRoom.label" default="End Date Room" /></span> <span
							class="property-value" aria-labelledby="endDateRoom-label"><g:formatDate
									date="${roomInstance?.endDateRoom}" /></span></li>
					</g:if>

					<g:if test="${roomInstance?.descriptionRoom}">
						<li class="fieldcontain"><span id="descriptionRoom-label"
							class="property-label"><g:message
									code="room.descriptionRoom.label" default="Description Room" /></span>

							<span class="property-value"
							aria-labelledby="descriptionRoom-label"><g:fieldValue
									bean="${roomInstance}" field="descriptionRoom" /></span></li>
					</g:if>

					<g:if test="${roomInstance?.idRoom}">
						<li class="fieldcontain"><span id="idRoom-label"
							class="property-label"><g:message code="room.idRoom.label"
									default="Id Room" /></span> <span class="property-value"
							aria-labelledby="idRoom-label"><g:fieldValue
									bean="${roomInstance}" field="idRoom" /></span></li>
					</g:if>

					<g:if test="${roomInstance?.reservations}">
						<li class="fieldcontain"><span id="reservations-label"
							class="property-label"><g:message
									code="room.reservations.label" default="Reservations" /></span> <g:each
								in="${roomInstance.reservations}" var="r">
								<span class="property-value"
									aria-labelledby="reservations-label"><g:link
										controller="reservation" action="show" id="${r.id}">
										${r?.encodeAsHTML()}
									</g:link></span>
							</g:each></li>
					</g:if>

					<g:if test="${roomInstance?.services}">
						<li class="fieldcontain"><span id="services-label"
							class="property-label"><g:message
									code="room.services.label" default="Services" /></span> <g:each
								in="${roomInstance.services}" var="s">
								<span class="property-value" aria-labelledby="services-label"><g:link
										controller="service" action="show" id="${s.id}">
										${s?.encodeAsHTML()}
									</g:link></span>
							</g:each></li>
					</g:if>

				</ol>
				<g:form url="[resource:roomInstance, action:'delete']"
					method="DELETE">
					<fieldset class="buttons">
						<g:if test="${session.rol == 'Admin'}">
							<g:link class="edit" action="edit" resource="${roomInstance}">
								<g:message code="default.button.edit.label" default="Edit" />
							</g:link>
						</g:if>
						<%--					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />--%>
					</fieldset>
				</g:form>
			</div>
		</div>
	</section>
</body>
</html>
