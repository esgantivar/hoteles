
<%@ page import="hoteleria.Room" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="hoteleriaLayout">
		<g:set var="entityName" value="${message(code: 'room.label', default: 'Room')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<section id="contact-page">
		<div class="container">
			<div class="center">
				<br /> <br />
				<h2>Habitaciones</h2>
				<p class="lead">Consulta las habitaciones</p>
		<div id="list-room" class="content scaffold-list" role="main">
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			
				<fieldset class="form">
			    	<g:form action="list" method="GET">
			        	<div class="fieldcontain">
			            	<label>Search for Price:</label><input type="number" name= "price"/><input type="number" name= "price1"/>
                            <label>&nbsp;</label><input type="submit" value="Search"/>
			        	</div>
			    	</g:form>
				</fieldset>
			<table border="1" style="text-align: center; width: 1000px">
			<thead>
					<tr>
					
						<g:sortableColumn property="numerOfPeople" title="${message(code: 'room.numerOfPeople.label', default: 'Numer Of People')}" />
					
						<g:sortableColumn property="priceRoom" title="${message(code: 'room.priceRoom.label', default: 'Price Room')}" />
					
						<g:sortableColumn property="startDateRoom" title="${message(code: 'room.startDateRoom.label', default: 'Start Date Room')}" />
					
						<g:sortableColumn property="endDateRoom" title="${message(code: 'room.endDateRoom.label', default: 'End Date Room')}" />
					
						<g:sortableColumn property="descriptionRoom" title="${message(code: 'room.descriptionRoom.label', default: 'Description Room')}" />
					
						<g:sortableColumn property="idRoom" title="${message(code: 'room.idRoom.label', default: 'Id Room')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${roomInstanceList}" status="i" var="roomInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${roomInstance.id}">${fieldValue(bean: roomInstance, field: "numerOfPeople")}</g:link></td>
					
						<td>${fieldValue(bean: roomInstance, field: "priceRoom")}</td>
					
						<td><g:formatDate date="${roomInstance.startDateRoom}" /></td>
					
						<td><g:formatDate date="${roomInstance.endDateRoom}" /></td>
					
						<td>${fieldValue(bean: roomInstance, field: "descriptionRoom")}</td>
					
						<td>${fieldValue(bean: roomInstance, field: "idRoom")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${roomInstanceCount ?: 0}" />
			</div>
		</div>
		</div>
		</div>
	</section>
	</body>
</html>
