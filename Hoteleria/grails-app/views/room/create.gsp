<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="hoteleriaLayout">
<g:set var="entityName"
	value="${message(code: 'room.label', default: 'Room')}" />
<title><g:message code="default.create.label"
		args="[entityName]" /></title>
</head>
<body>
	<section id="contact-page">
		<div class="container">
			<div class="center">
				<br /> <br />
				<h2>Crear Habitacion</h2>
				<p class="lead">Ingresa los datos de la habitacion</p>
				<p class="lead" style="color: red">
					<g:hasErrors bean="${roomInstance}">
						<ul class="errors" role="alert">
							<g:eachError bean="${roomInstance}" var="error">
								<li
									<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
										error="${error}" /></li>
							</g:eachError>
						</ul>
					</g:hasErrors>
				</p>
			</div>
			<div class="row contact-wrap">
				<div class="status alert alert-success" style="display: none"></div>
				<g:form url="[resource:roomInstance, action:'save']">
					<div class="col-sm-5 col-sm-offset-3">
						<g:render template="form" />
						<div class="form-group">
							<g:submitButton name="create" class="btn btn-primary btn-lg"
								value="${message(code: 'default.button.create.label', default: 'Create')}" />
						</div>
					</div>
				</g:form>
			</div>
		</div>
	</section>
</body>
</html>
