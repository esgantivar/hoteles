<%@ page import="hoteleria.Room" %>



<div class="fieldcontain ${hasErrors(bean: roomInstance, field: 'numerOfPeople', 'error')} required">
	<label for="numerOfPeople">
		<g:message code="room.numerOfPeople.label" default="Numer Of People" />
		<span class="required-indicator">*</span>
	</label>
	<g:field class="form-control" name="numerOfPeople" type="number" min="1" value="${roomInstance.numerOfPeople}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: roomInstance, field: 'priceRoom', 'error')} required">
	<label for="priceRoom">
		<g:message code="room.priceRoom.label" default="Price Room" />
		<span class="required-indicator">*</span>
	</label>
	<g:field class="form-control" name="priceRoom" value="${fieldValue(bean: roomInstance, field: 'priceRoom')}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: roomInstance, field: 'startDateRoom', 'error')} required">
	<label for="startDateRoom">
		<g:message code="room.startDateRoom.label" default="Start Date Room" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker class="form-control" name="startDateRoom" precision="day"  value="${roomInstance?.startDateRoom}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: roomInstance, field: 'endDateRoom', 'error')} required">
	<label for="endDateRoom">
		<g:message code="room.endDateRoom.label" default="End Date Room" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker class="form-control" name="endDateRoom" precision="day"  value="${roomInstance?.endDateRoom}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: roomInstance, field: 'descriptionRoom', 'error')} required">
	<label for="descriptionRoom">
		<g:message code="room.descriptionRoom.label" default="Description Room" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField class="form-control" name="descriptionRoom" required="" value="${roomInstance?.descriptionRoom}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: roomInstance, field: 'idRoom', 'error')} required">
	<label for="idRoom">
		<g:message code="room.idRoom.label" default="Id Room" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField class="form-control" name="idRoom" required="" value="${roomInstance?.idRoom}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: roomInstance, field: 'reservations', 'error')} ">
	<label for="reservations">
		<g:message code="room.reservations.label" default="Reservations" />
		
	</label>
	<g:select class="form-control" name="reservations" from="${hoteleria.Reservation.list()}" multiple="multiple" optionKey="id" size="5" value="${roomInstance?.reservations*.id}" class="many-to-many"/>

</div>

<div class="fieldcontain ${hasErrors(bean: roomInstance, field: 'services', 'error')} ">
	<label for="services">
		<g:message code="room.services.label" default="Services" />
		
	</label>
	<g:select class="form-control" name="services" from="${hoteleria.Service.list()}" multiple="multiple" optionKey="id" size="5" value="${roomInstance?.services*.id}" class="many-to-many"/>

</div>

