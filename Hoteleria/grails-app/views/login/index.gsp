<html>
<head lang="es">
<meta name="layout" content="hoteleriaLayout" />
<title>Login</title>
</head>
<body>
	<section id="contact-page">
		<div class="container">
			<div class="center">
				<br /> <br />
				<h2>Iniciar Sesión</h2>
				<p class="lead">Puedes ingresar desde aqui con tus datos.</p>
				<g:if test="${flash.message}">
					<p class="lead" style="color: red">
						${flash.message}
					</p>
				</g:if>
				<g:if test="${params.mensaje != null}">
					<p class="lead" style="color: green;">
						${params.mensaje}
					</p>
				</g:if>
			</div>
			<div class="row contact-wrap">
				<div class="status alert alert-success" style="display: none"></div>

				<g:form action="login" id="main-contact-form" name="contact-form"
					method="post">
					<div class="col-sm-5 col-sm-offset-3">
						<div class="form-group">
							<label>Email *</label> <input type="email" name="email"
								class="form-control" required="required">
						</div>
						<div class="form-group">
							<label>Contraseña *</label> <input type="password"
								name="password" class="form-control" required="required">
						</div>
						<div class="form-group">
							<button type="submit" name="submit"
								class="btn btn-primary btn-lg" required="required">Ingresar</button>
							<button type="button" class="btn btn-primary btn-lg"
								onclick="enviarFormulario(document,'formRegistro')">Registrarse</button>
						</div>
					</div>
				</g:form>
			</div>

			<!--/.row-->
		</div>
		<!--/.container-->
	</section>
	<!--/#contact-page-->

	<g:form id="formRegistro" name="formRegistro" controller="User"
		action="create" style="padding-left:5px">
	</g:form>
</body>
</html>
