
<%@ page contentType="text/html;charset=UTF-8" %>

<html>
    <head>
        <meta name="layout" content="main"/>
        <title>Login</title>
        </head>
        <body>
            
            <g:form controller ="User" action="create" style="padding-left:5px">
                    <label>&nbsp;</label><input type="submit" value="Register"/>
            </g:form>
            <g:if test="${flash.message}">
                <div class="message">
                    ${flash.message}
                </div>
            </g:if>
            <g:if test="${session.user}">
                <br/>
                   You are login as : ${session.user} | Logout <g:link action="logout"> Logout </g:link>
            </g:if>
            <g:else>
                <g:form action="login" style="padding-left:200px">
                    <div style="width:10px">
                        <label>Email:</label><input type="text" name="email"/>
                        <label>Password:</label><input type="password" name= "password"/>
                        <label>&nbsp;</label><input type="submit" value="login"/>
                    </div>
                </g:form>
            </g:else>
        </body>
</html>
