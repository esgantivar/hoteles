<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
<%--		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">--%>
<%--		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">--%>
<%--		<title><g:layoutTitle default="Grails"/></title>--%>
<%--		<meta name="viewport" content="width=device-width, initial-scale=1.0">--%>
<%--		<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">--%>
<%--		<link rel="apple-touch-icon" href="${assetPath(src: 'apple-touch-icon.png')}">--%>
<%--		<link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'apple-touch-icon-retina.png')}">--%>
<%--  		<asset:stylesheet src="application.css"/>--%>
<%--		<asset:javascript src="application.js"/>--%>
<%--		<g:layoutHead/>--%>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
			<title><g:layoutTitle default="Hotel"/></title>
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
<%--			<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">--%>
			<link rel="shortcut icon" href="${assetPath(src: 'favicon2.ico')}" type="image/x-icon">
<%--  		<asset:stylesheet src="application.css"/>--%>
<%--		<asset:javascript src="application.js"/>--%>
			<!-- core CSS -->
			<asset:stylesheet src="bootstrap.min.css" />
			<asset:stylesheet src="font-awesome.min.css" />
			<asset:stylesheet src="animate.min.css" />
			<asset:stylesheet src="prettyPhoto.css" />
			<asset:stylesheet src="main.css" />
			<asset:stylesheet src="responsive.css" />
			<asset:javascript src="jquery.js" />
			
			
			<asset:javascript src="bootstrap.min.js" />
			<asset:javascript src="jquery.prettyPhoto.js" />
			<asset:javascript src="jquery.isotope.min.js" />
			<asset:javascript src="main.js" />
			<asset:javascript src="wow.min.js" />
			
		    <script src="js/jquery.js"></script>
		    <script src="js/bootstrap.min.js"></script>
		    <script src="js/jquery.prettyPhoto.js"></script>
		    <script src="js/jquery.isotope.min.js"></script>
		    <script src="js/main.js"></script>
		    <script src="js/wow.min.js"></script>
		
		    <!--[if lt IE 9]>
		    
		    <asset:javascript src="html5shiv.js" />
		    <asset:javascript src="respond.min.js" />
		        
		    <![endif]-->  
			
			<div class="top-bar">
		            <div class="container">
		                <div class="row">
		                    <div class="col-sm-6 col-xs-4">
		                        <div class="top-number"><p><i class="fa fa-phone-square"></i>  +0123 456 70 90</p></div>
		                    </div>
		                    <div class="col-sm-6 col-xs-8">
		                       <div class="social">
		                            <ul class="social-share">
		                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
		                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
		                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li> 
		                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
		                                <li><a href="#"><i class="fa fa-skype"></i></a></li>
		                            </ul>
		                            <div class="search">
		                                <form role="form">
		                                    <input type="text" class="search-form" autocomplete="off" placeholder="Search">
		                                    <i class="fa fa-search"></i>
		                                </form>
		                           </div>
		                       </div>
		                    </div>
		                </div>
		            </div><!--/.container-->
		        </div><!--/.top-bar-->
		    	 
		        <nav class="navbar navbar-inverse" role="banner">
		            <div class="container">
		                <div class="navbar-header">
		                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		                        <span class="sr-only">Toggle navigation</span>
		                        <span class="icon-bar"></span>
		                        <span class="icon-bar"></span>
		                        <span class="icon-bar"></span>
		                    </button>
		                    <a class="navbar-brand" href="index.html"><img src="images/logo.png" alt="logo"></a>
		                </div>
						
		                <div class="collapse navbar-collapse navbar-right">
		                    <ul class="nav navbar-nav">
		                        <li class="active"><a href="index.html">Home</a></li>
		                        <li><a href="about-us.html">About Us</a></li>
		                        <li><a href="services.html">Services</a></li>
		                        <li><a href="portfolio.html">Portfolio</a></li>
		                        <li class="dropdown">
		                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pages <i class="fa fa-angle-down"></i></a>
		                            <ul class="dropdown-menu">
		                                <li><a href="blog-item.html">Blog Single</a></li>
		                                <li><a href="pricing.html">Pricing</a></li>
		                                <li><a href="404.html">404</a></li>
		                                <li><a href="shortcodes.html">Shortcodes</a></li>
		                            </ul>
		                        </li>
		                        <li><a href="blog.html">Blog</a></li> 
		                        <li><a href="contact-us.html">Contact</a></li>                        
		                    </ul>
		                </div>
		            </div><!--/.container-->
		        </nav><!--/nav-->
		<g:layoutHead/>
	</head>
	<body>
<%--		<div id="grailsLogo" role="banner"><a href="http://grails.org"><asset:image src="grails_logo.png" alt="Grails"/></a></div>--%>
		<g:layoutBody/>
<%--		<div class="footer" role="contentinfo"></div>--%>
<%--		<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>--%>
		<footer id="footer" class="midnight-blue">
	        <div class="container">
	            <div class="row">
	                <div class="col-sm-6">
	                    &copy; 2013 <a target="_blank" href="http://shapebootstrap.net/" title="Free Twitter Bootstrap WordPress Themes and HTML templates">ShapeBootstrap</a>. All Rights Reserved.
	                </div>
	                <div class="col-sm-6">
	                    <ul class="pull-right">
	                        <li><a href="#">Home</a></li>
	                        <li><a href="#">About Us</a></li>
	                        <li><a href="#">Faq</a></li>
	                        <li><a href="#">Contact Us</a></li>
	                    </ul>
	                </div>
	            </div>
	        </div>
    	</footer><!--/#footer-->
	</body>
</html>
