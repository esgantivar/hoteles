<%@ page contentType="text/html;charset=UTF-8"%>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title><g:layoutTitle default="Hoteleria" /></title>
<link href="${resource(dir: 'css', file: 'bootstrap.min.css')}"
	rel="stylesheet">
<link href="${resource(dir: 'css', file: 'font-awesome.min.css')}"
	rel="stylesheet">
<link href="${resource(dir: 'css', file: 'prettyPhoto.css')}"
	rel="stylesheet">
<link href="${resource(dir: 'css', file: 'animate.min.css')}"
	rel="stylesheet">
<link href="${resource(dir: 'css', file: 'main.css')}" rel="stylesheet">
<link href="${resource(dir: 'css', file: 'responsive.css')}"
	rel="stylesheet">
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
<link rel="shortcut icon"
	href="${resource(dir: 'images/ico', file: 'imagesfavicon.ico')}">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="${resource(dir: 'images/ico', file: 'apple-touch-icon-144-precomposed.png')}">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="${resource(dir: 'images/ico', file: 'apple-touch-icon-114-precomposed.png')}">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="${resource(dir: 'images/ico', file: 'apple-touch-icon-72-precomposed.png')}">
<link rel="apple-touch-icon-precomposed"
	href="${resource(dir: 'images/ico', file: 'apple-touch-icon-57-precomposed.png')}">
<g:layoutHead />
</head>
<!--/head-->
<body>
	<header id="header">
		<div class="top-bar">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-xs-4">
						<div class="top-number">
							<p>
								<i class="fa fa-phone-square"></i> +0123 456 70 90
							</p>
						</div>
					</div>
					<div class="col-sm-6 col-xs-8">
						<div class="social">
							<div class="search">
								<g:if test="${session.user != null}">
									<div style="color: white;">
										<strong>Hola! ${session.user} &nbsp; &nbsp;
										</strong>
									</div>
								</g:if>
							</div>
							<ul class="social-share">
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
								<li><a href="#"><i class="fa fa-skype"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!--/.container-->
		</div>
		<!--/.top-bar-->

		<nav class="navbar navbar-inverse" role="banner">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.html"><img
						src="${resource(dir: 'images', file: 'logo.png')}" alt="logo"></a>
				</div>

				<div class="collapse navbar-collapse navbar-right">
					<ul class="nav navbar-nav">
						<li><a href="/Hoteleria/">Home</a></li>
						<g:if test="${session.user != null}">

							<g:if test="${session.rol == 'Admin'}">

								<li class="dropdown"><a href="#"
									class="dropdown-toggle" data-toggle="dropdown">Hoteles<i
										class="fa fa-angle-down"></i></a>
									<ul class="dropdown-menu">
										<li><a style="cursor: pointer;"
											onclick="enviarFormulario(document,'formHotel')">Consultar</a></li>
										<li><a style="cursor: pointer;"
											onclick="enviarFormulario(document,'formHotelCrear')">Crear</a></li>
									</ul></li>

								<li class="dropdown"><a href="#"
									class="dropdown-toggle" data-toggle="dropdown">Servicios<i
										class="fa fa-angle-down"></i></a>
									<ul class="dropdown-menu">
										<li><a style="cursor: pointer;"
											onclick="enviarFormulario(document,'formServicio')">Consultar</a></li>
										<li><a style="cursor: pointer;"
											onclick="enviarFormulario(document,'formServicioCrear')">Crear</a></li>
									</ul></li>

								<li class="dropdown"><a href="#"
									class="dropdown-toggle" data-toggle="dropdown">Habitaciones<i
										class="fa fa-angle-down"></i></a>
									<ul class="dropdown-menu">
										<li><a style="cursor: pointer;"
											onclick="enviarFormulario(document,'formHabitacion')">Consultar</a></li>
										<li><a style="cursor: pointer;"
											onclick="enviarFormulario(document,'formHabitacionCrear')">Crear</a></li>
									</ul></li>

								<li class="dropdown"><a href="#"
									class="dropdown-toggle" data-toggle="dropdown">Reservaciones<i
										class="fa fa-angle-down"></i></a>
									<ul class="dropdown-menu">
										<li><a style="cursor: pointer;"
											onclick="enviarFormulario(document,'formReservacion')">Consultar</a></li>
										<li><a style="cursor: pointer;"
											onclick="enviarFormulario(document,'formReservacionCrear')">Crear</a></li>
									</ul></li>

							</g:if>

							<g:if test="${session.rol == 'User'}">
								<li><a style="cursor: pointer;"
									onclick="enviarFormulario(document,'formHotel')">Hoteles</a></li>
								<li><a style="cursor: pointer;"
									onclick="enviarFormulario(document,'formReservacionCrear')">Reservar</a></li>
							</g:if>

							<li><a style="cursor: pointer;"
								onclick="enviarFormulario(document,'formSalir')">Salir</a></li>

						</g:if>

						<%--						<li><a href="about-us.html">About Us</a></li>--%>
						<%--						<li><a href="services.html">Services</a></li>--%>
						<%--						<li><a href="portfolio.html">Portfolio</a></li>--%>
						<%--						<li class="dropdown active"><a href="#"--%>
						<%--							class="dropdown-toggle" data-toggle="dropdown">Pages <i--%>
						<%--								class="fa fa-angle-down"></i></a>--%>
						<%--							<ul class="dropdown-menu">--%>
						<%--								<li><a href="blog-item.html">Blog Single</a></li>--%>
						<%--								<li><a href="pricing.html">Pricing</a></li>--%>
						<%--								<li class="active"><a href="404.html">404</a></li>--%>
						<%--								<li><a href="shortcodes.html">Shortcodes</a></li>--%>
						<%--							</ul></li>--%>
						<%--						<li><a href="blog.html">Blog</a></li>--%>
						<%--						<li><a href="contact-us.html">Contact</a></li>--%>
					</ul>
				</div>
			</div>
			<!--/.container-->
		</nav>
		<!--/nav-->

	</header>
	<!--/header-->
	<g:layoutBody />

	<section id="bottom">
		<div class="container wow fadeInDown" data-wow-duration="1000ms"
			data-wow-delay="600ms">
			<div class="row">
				<%--                <div class="col-md-3 col-sm-6">--%>
				<%--                    <div class="widget">--%>
				<%--                        <h3>Company</h3>--%>
				<%--                        <ul>--%>
				<%--                            <li><a href="#">About us</a></li>--%>
				<%--                            <li><a href="#">We are hiring</a></li>--%>
				<%--                            <li><a href="#">Meet the team</a></li>--%>
				<%--                            <li><a href="#">Copyright</a></li>--%>
				<%--                            <li><a href="#">Terms of use</a></li>--%>
				<%--                            <li><a href="#">Privacy policy</a></li>--%>
				<%--                            <li><a href="#">Contact us</a></li>--%>
				<%--                        </ul>--%>
				<%--                    </div>    --%>
				<%--                </div><!--/.col-md-3-->--%>
				<%----%>
				<%--                <div class="col-md-3 col-sm-6">--%>
				<%--                    <div class="widget">--%>
				<%--                        <h3>Support</h3>--%>
				<%--                        <ul>--%>
				<%--                            <li><a href="#">Faq</a></li>--%>
				<%--                            <li><a href="#">Blog</a></li>--%>
				<%--                            <li><a href="#">Forum</a></li>--%>
				<%--                            <li><a href="#">Documentation</a></li>--%>
				<%--                            <li><a href="#">Refund policy</a></li>--%>
				<%--                            <li><a href="#">Ticket system</a></li>--%>
				<%--                            <li><a href="#">Billing system</a></li>--%>
				<%--                        </ul>--%>
				<%--                    </div>    --%>
				<%--                </div><!--/.col-md-3-->--%>

				<div class="col-md-3 col-sm-6">
					<div class="widget">
						<h3>Developers</h3>
						<ul>
							<li><a href="#">Alexa Avendaño</a></li>
							<li><a href="#">Sneyder Gantiva</a></li>
							<li><a href="#">Cesar Galvis</a></li>
							<li><a href="#">Wiston Forero</a></li>
							<li><a href="#">Julio Sanchez</a></li>
						</ul>
					</div>
				</div>
				<!--/.col-md-3-->

				<%--                <div class="col-md-3 col-sm-6">--%>
				<%--                    <div class="widget">--%>
				<%--                        <h3>Our Partners</h3>--%>
				<%--                        <ul>--%>
				<%--                            <li><a href="#">Adipisicing Elit</a></li>--%>
				<%--                            <li><a href="#">Eiusmod</a></li>--%>
				<%--                            <li><a href="#">Tempor</a></li>--%>
				<%--                            <li><a href="#">Veniam</a></li>--%>
				<%--                            <li><a href="#">Exercitation</a></li>--%>
				<%--                            <li><a href="#">Ullamco</a></li>--%>
				<%--                            <li><a href="#">Laboris</a></li>--%>
				<%--                        </ul>--%>
				<%--                    </div>    --%>
				<%--                </div><!--/.col-md-3-->--%>
			</div>
		</div>
	</section>
	<!--/#bottom-->

	<footer id="footer" class="midnight-blue">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					&copy; 2013 <a target="_blank" href="http://shapebootstrap.net/"
						title="Free Twitter Bootstrap WordPress Themes and HTML templates">ShapeBootstrap</a>.
					All Rights Reserved.
				</div>
				<div class="col-sm-6">
					<ul class="pull-right">
						<li><a href="#">Home</a></li>
						<li><a href="#">About Us</a></li>
						<li><a href="#">Faq</a></li>
						<li><a href="#">Contact Us</a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
	<!--/#footer-->

	<g:form id="formSalir" name="formSalir" controller="Login"
		action="logout">
	</g:form>

	<g:form id="formHoteles" name="formHotel" controller="Hotel"
		action="index">
	</g:form>
	
	<g:form id="formHoteles" name="formHotelCrear" controller="Hotel"
		action="create">
	</g:form>
	
	<g:form id="formHoteles" name="formServicio" controller="Service"
		action="index">
	</g:form>
	
	<g:form id="formHoteles" name="formServicioCrear" controller="Service"
		action="create">
	</g:form>
	
	<g:form id="formHoteles" name="formHabitacion" controller="Room"
		action="index">
	</g:form>
	
	<g:form id="formHoteles" name="formHabitacionCrear" controller="Room"
		action="create">
	</g:form>
	
	<g:form id="formHoteles" name="formReservacion" controller="Reservation"
		action="index">
	</g:form>
	
	<g:form id="formHoteles" name="formReservacionCrear" controller="Reservation"
		action="create">
	</g:form>

	<script src="${resource(dir: 'js', file: 'jquery.js')}"></script>
	<script src="${resource(dir: 'js', file: 'bootstrap.min.js')}"></script>
	<script src="${resource(dir: 'js', file: 'jquery.prettyPhoto.js')}"></script>
	<script src="${resource(dir: 'js', file: 'jquery.isotope.min.js')}"></script>
	<script src="${resource(dir: 'js', file: 'main.js')}"></script>
	<script src="${resource(dir: 'js', file: 'wow.min.js')}"></script>
	<script src="${resource(dir: 'js', file: 'utilidades.js')}"></script>
</body>
</html>
