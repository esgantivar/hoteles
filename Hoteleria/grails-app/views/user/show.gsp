
<%@ page import="hoteleria.User"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="hoteleriaLayout">
<g:set var="entityName"
	value="${message(code: 'user.label', default: 'User')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<section id="contact-page">
		<div class="container">

			<div class="left">
				<br /> <br />
				<h2>Datos del Usuario</h2>
				<g:if test="${flash.message}">
					<div class="message" role="status">
						${flash.message}
					</div>
				</g:if>
				<ol class="property-list user">

					<g:if test="${userInstance?.userName}">
						<li class="fieldcontain"><span id="userName-label"
							class="property-label"><g:message
									code="user.userName.label" default="User Name" /></span> <span
							class="property-value" aria-labelledby="userName-label"><g:fieldValue
									bean="${userInstance}" field="userName" /></span></li>
					</g:if>

					<g:if test="${userInstance?.password}">
						<li class="fieldcontain"><span id="password-label"
							class="property-label"><g:message
									code="user.password.label" default="Password" /></span> <span
							class="property-value" aria-labelledby="password-label"><g:fieldValue
									bean="${userInstance}" field="password" /></span></li>
					</g:if>

					<g:if test="${userInstance?.email}">
						<li class="fieldcontain"><span id="email-label"
							class="property-label"><g:message code="user.email.label"
									default="Email" /></span> <span class="property-value"
							aria-labelledby="email-label"><g:fieldValue
									bean="${userInstance}" field="email" /></span></li>
					</g:if>

					<g:if test="${userInstance?.telephone}">
						<li class="fieldcontain"><span id="telephone-label"
							class="property-label"><g:message
									code="user.telephone.label" default="Telephone" /></span> <span
							class="property-value" aria-labelledby="telephone-label"><g:fieldValue
									bean="${userInstance}" field="telephone" /></span></li>
					</g:if>

					<g:if test="${userInstance?.address}">
						<li class="fieldcontain"><span id="address-label"
							class="property-label"><g:message
									code="user.address.label" default="Address" /></span> <span
							class="property-value" aria-labelledby="address-label"><g:fieldValue
									bean="${userInstance}" field="address" /></span></li>
					</g:if>

					<g:if test="${userInstance?.reserves}">
						<li class="fieldcontain"><span id="reserves-label"
							class="property-label"><g:message
									code="user.reserves.label" default="Reserves" /></span> <g:each
								in="${userInstance.reserves}" var="r">
								<span class="property-value" aria-labelledby="reserves-label"><g:link
										controller="reservation" action="show" id="${r.id}">
										${r?.encodeAsHTML()}
									</g:link></span>
							</g:each></li>
					</g:if>

				</ol>
				<g:form url="[resource:userInstance, action:'delete']"
					method="DELETE">
					<fieldset class="buttons">
						<g:if test="${session.rol == 'Admin'}">
							<g:link class="edit" action="edit" resource="${userInstance}">
								<g:message code="default.button.edit.label" default="Edit" />
							</g:link>
						</g:if>
						<%--					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />--%>
					</fieldset>
				</g:form>
			</div>
		</div>
	</section>
</body>
</html>
