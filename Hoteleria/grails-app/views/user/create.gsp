<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="hoteleriaLayout">
<title>Registro de usuario</title>
</head>

<body>
	<section id="contact-page">
		<div class="container">
			<div class="center">
				<br /> <br />
				<h2>Registrate</h2>
				<p class="lead">Ingresa tus datos para registrarte</p>
				<p class="lead" style="color: red">
					<g:hasErrors bean="${userInstance}">
						<g:eachError bean="${userInstance}" var="error">
							<li style="color: red"
								<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
									error="${error}" /></li>
						</g:eachError>
					</g:hasErrors>
				</p>
			</div>
			<div class="row contact-wrap">
				<div class="status alert alert-success" style="display: none"></div>
				<g:form url="[resource:userInstance, action:'save']"
					name="contact-form">
					<div class="col-sm-5 col-sm-offset-3">
						<g:render template="form" />
						<div class="form-group">
							<g:submitButton class="btn btn-primary btn-lg" name="create"
								value="Registrarse" />
						</div>
					</div>
				</g:form>
			</div>

			<!--/.row-->
		</div>
		<!--/.container-->
	</section>
	<!--/#contact-page-->

	<g:form id="formRegistro" name="formRegistro" controller="User"
		action="create" style="padding-left:5px">
	</g:form>
</body>

<%--	<body>--%>
<%--		<a href="#create-user" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>--%>
<%--		<div class="nav" role="navigation">--%>
<%--			<ul>--%>
<%--				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--%>
<%--				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>--%>
<%--			</ul>--%>
<%--		</div>--%>
<%--		<div id="create-user" class="content scaffold-create" role="main">--%>
<%--			<h1><g:message code="default.create.label" args="[entityName]" /></h1>--%>
<%--			<g:if test="${flash.message}">--%>
<%--			<div class="message" role="status">${flash.message}</div>--%>
<%--			</g:if>--%>
<%--			<g:hasErrors bean="${userInstance}">--%>
<%--			<ul class="errors" role="alert">--%>
<%--				<g:eachError bean="${userInstance}" var="error">--%>
<%--				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>--%>
<%--				</g:eachError>--%>
<%--			</ul>--%>
<%--			</g:hasErrors>--%>
<%--			<g:form url="[resource:userInstance, action:'save']" method="POST">--%>
<%--				<fieldset class="form">--%>
<%--					<g:render template="form"/>--%>
<%--				</fieldset>--%>
<%--				<fieldset class="buttons">--%>
<%--					<g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />--%>
<%--				</fieldset>--%>
<%--			</g:form>--%>
<%--		</div>--%>
<%--	</body>--%>
</html>
