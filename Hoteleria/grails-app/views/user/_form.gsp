<%@ page import="hoteleria.User"%>



<div
	class="fieldcontain ${hasErrors(bean: userInstance, field: 'userName', 'error')} required">
	<label for="userName"> <g:message code="user.userName.label"
			default="User Name" /> <span class="required-indicator">*</span>
	</label>
	<g:textField class="form-control" name="userName" maxlength="20"
		required="" value="${userInstance?.userName}" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: userInstance, field: 'password', 'error')} required">
	<label for="password"> <g:message code="user.password.label"
			default="Password" /> <span class="required-indicator">*</span>
	</label>
	<g:field class="form-control" type="password" name="password"
		maxlength="20" required="" value="${userInstance?.password}" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: userInstance, field: 'email', 'error')} required">
	<label for="email"> <g:message code="user.email.label"
			default="Email" /> <span class="required-indicator">*</span>
	</label>
	<g:field class="form-control" type="email" name="email" required=""
		value="${userInstance?.email}" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: userInstance, field: 'adress', 'error')} required">
	<label for="adress"> <g:message code="user.adress.label"
			default="Address" /> <span class="required-indicator">*</span>
	</label>
	<g:textField class="form-control" name="address" required=""
		value="${userInstance?.address}" />

</div>

<%--<div--%>
<%--	class="fieldcontain ${hasErrors(bean: userInstance, field: 'reserves', 'error')} ">--%>
<%--	<label for="reserves"> <g:message code="user.reserves.label"--%>
<%--			default="Reserves" />--%>
<%----%>
<%--	</label>--%>
<%--	<ul class="one-to-many">--%>
<%--		<g:each in="${userInstance?.reserves?}" var="r">--%>
<%--			<li><g:link controller="reservation" action="show" id="${r.id}">--%>
<%--					${r?.encodeAsHTML()}--%>
<%--				</g:link></li>--%>
<%--		</g:each>--%>
<%--		<li class="add"><g:link controller="reservation" action="create"--%>
<%--				params="['user.id': userInstance?.id]">--%>
<%--				${message(code: 'default.add.label', args: [message(code: 'reservation.label', default: 'Reservation')])}--%>
<%--			</g:link></li>--%>
<%--	</ul>--%>
<%----%>
<%----%>
<%--</div>--%>

<div
	class="fieldcontain ${hasErrors(bean: userInstance, field: 'telephone', 'error')} required">
	<label for="telephone"> <g:message code="user.telephone.label"
			default="Telephone" /> <span class="required-indicator">*</span>
	</label>
	<g:textField class="form-control" name="telephone" required=""
		value="${userInstance?.telephone}" />

</div>

