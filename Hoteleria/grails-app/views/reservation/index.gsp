
<%@ page import="hoteleria.Reservation" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="hoteleriaLayout">
		<g:set var="entityName" value="${message(code: 'reservation.label', default: 'Reservation')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<section id="contact-page">
		<div class="container">
			<div class="center">
				<br /> <br />
				<h2>Reservaciones</h2>
				<p class="lead">Consulta las Reservaciones</p>
		<div id="list-reservation" class="content scaffold-list" role="main">
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table border="1" style="text-align: center; width: 1000px">
			<thead>
					<tr>
					
						<g:sortableColumn property="idReserv" title="${message(code: 'reservation.idReserv.label', default: 'Id Reserv')}" />
					
						<g:sortableColumn property="emisionDateReserv" title="${message(code: 'reservation.emisionDateReserv.label', default: 'Emision Date Reserv')}" />
					
						<g:sortableColumn property="invalidDateReserv" title="${message(code: 'reservation.invalidDateReserv.label', default: 'Invalid Date Reserv')}" />
					
						<g:sortableColumn property="totalPriceReserv" title="${message(code: 'reservation.totalPriceReserv.label', default: 'Total Price Reserv')}" />
					
						<g:sortableColumn property="pay" title="${message(code: 'reservation.pay.label', default: 'Pay')}" />
					
						<th><g:message code="reservation.hotel.label" default="Hotel" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${reservationInstanceList}" status="i" var="reservationInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${reservationInstance.id}">${fieldValue(bean: reservationInstance, field: "idReserv")}</g:link></td>
					
						<td><g:formatDate date="${reservationInstance.emisionDateReserv}" /></td>
					
						<td><g:formatDate date="${reservationInstance.invalidDateReserv}" /></td>
					
						<td>${fieldValue(bean: reservationInstance, field: "totalPriceReserv")}</td>
					
						<td>${fieldValue(bean: reservationInstance, field: "pay")}</td>
					
						<td>${fieldValue(bean: reservationInstance, field: "hotel")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${reservationInstanceCount ?: 0}" />
			</div>
		</div>
		</div>
		</div>
	</section>
	</body>
</html>
