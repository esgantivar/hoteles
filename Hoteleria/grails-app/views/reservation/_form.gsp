<%@ page import="hoteleria.Reservation" %>



<div class="fieldcontain ${hasErrors(bean: reservationInstance, field: 'idReserv', 'error')} required">
	<label for="idReserv">
		<g:message code="reservation.idReserv.label" default="Id Reserv" />
		<span class="required-indicator">*</span>
	</label>
	<g:field class="form-control" name="idReserv" type="number" value="${reservationInstance.idReserv}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: reservationInstance, field: 'emisionDateReserv', 'error')} required">
	<label for="emisionDateReserv">
		<g:message code="reservation.emisionDateReserv.label" default="Emision Date Reserv" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker class="form-control" name="emisionDateReserv" precision="day"  value="${reservationInstance?.emisionDateReserv}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: reservationInstance, field: 'invalidDateReserv', 'error')} required">
	<label for="invalidDateReserv">
		<g:message code="reservation.invalidDateReserv.label" default="Invalid Date Reserv" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker class="form-control" name="invalidDateReserv" precision="day"  value="${reservationInstance?.invalidDateReserv}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: reservationInstance, field: 'totalPriceReserv', 'error')} required">
	<label for="totalPriceReserv">
		<g:message code="reservation.totalPriceReserv.label" default="Total Price Reserv" />
		<span class="required-indicator">*</span>
	</label>
	<g:field class="form-control" name="totalPriceReserv" value="${fieldValue(bean: reservationInstance, field: 'totalPriceReserv')}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: reservationInstance, field: 'pay', 'error')} required">
	<label for="pay">
		<g:message code="reservation.pay.label" default="Pay" />
		<span class="required-indicator">*</span>
	</label>
	<g:select class="form-control" name="pay" from="${reservationInstance.constraints.pay.inList}" required="" value="${reservationInstance?.pay}" valueMessagePrefix="reservation.pay"/>

</div>

<div class="fieldcontain ${hasErrors(bean: reservationInstance, field: 'hotel', 'error')} required">
	<label for="hotel">
		<g:message code="reservation.hotel.label" default="Hotel" />
		<span class="required-indicator">*</span>
	</label>
	<g:select class="form-control" id="hotel" name="hotel.id" from="${hoteleria.Hotel.list()}" optionKey="id" required="" value="${reservationInstance?.hotel?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: reservationInstance, field: 'user', 'error')} required">
	<label for="user">
		<g:message code="reservation.user.label" default="User" />
		<span class="required-indicator">*</span>
	</label>
	<g:select class="form-control" id="user" name="user.id" from="${hoteleria.User.list()}" optionKey="id" required="" value="${reservationInstance?.user?.id}" class="many-to-one"/>
<%--<input type="hidden" id="user" name="user.id" value="${session.userid }">--%>
</div>

