
<%@ page import="hoteleria.Reservation"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="hoteleriaLayout">
<g:set var="entityName"
	value="${message(code: 'reservation.label', default: 'Reservation')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<section id="contact-page">
		<div class="container">

			<div class="left">
				<br /> <br />
				<h2>Datos de la Reservacion</h2>
				<g:if test="${flash.message}">
					<div class="message" role="status">
						${flash.message}
					</div>
				</g:if>
				<ol class="property-list reservation">

					<g:if test="${reservationInstance?.idReserv}">
						<li class="fieldcontain"><span id="idReserv-label"
							class="property-label"><g:message
									code="reservation.idReserv.label" default="Id Reserv" /></span> <span
							class="property-value" aria-labelledby="idReserv-label"><g:fieldValue
									bean="${reservationInstance}" field="idReserv" /></span></li>
					</g:if>

					<g:if test="${reservationInstance?.emisionDateReserv}">
						<li class="fieldcontain"><span id="emisionDateReserv-label"
							class="property-label"><g:message
									code="reservation.emisionDateReserv.label"
									default="Emision Date Reserv" /></span> <span class="property-value"
							aria-labelledby="emisionDateReserv-label"><g:formatDate
									date="${reservationInstance?.emisionDateReserv}" /></span></li>
					</g:if>

					<g:if test="${reservationInstance?.invalidDateReserv}">
						<li class="fieldcontain"><span id="invalidDateReserv-label"
							class="property-label"><g:message
									code="reservation.invalidDateReserv.label"
									default="Invalid Date Reserv" /></span> <span class="property-value"
							aria-labelledby="invalidDateReserv-label"><g:formatDate
									date="${reservationInstance?.invalidDateReserv}" /></span></li>
					</g:if>

					<g:if test="${reservationInstance?.totalPriceReserv}">
						<li class="fieldcontain"><span id="totalPriceReserv-label"
							class="property-label"><g:message
									code="reservation.totalPriceReserv.label"
									default="Total Price Reserv" /></span> <span class="property-value"
							aria-labelledby="totalPriceReserv-label"><g:fieldValue
									bean="${reservationInstance}" field="totalPriceReserv" /></span></li>
					</g:if>

					<g:if test="${reservationInstance?.pay}">
						<li class="fieldcontain"><span id="pay-label"
							class="property-label"><g:message
									code="reservation.pay.label" default="Pay" /></span> <span
							class="property-value" aria-labelledby="pay-label"><g:fieldValue
									bean="${reservationInstance}" field="pay" /></span></li>
					</g:if>

					<g:if test="${reservationInstance?.hotel}">
						<li class="fieldcontain"><span id="hotel-label"
							class="property-label"><g:message
									code="reservation.hotel.label" default="Hotel" /></span> <span
							class="property-value" aria-labelledby="hotel-label"><g:link
									controller="hotel" action="show"
									id="${reservationInstance?.hotel?.id}">
									${reservationInstance?.hotel?.encodeAsHTML()}
								</g:link></span></li>
					</g:if>

					<g:if test="${reservationInstance?.user}">
						<li class="fieldcontain"><span id="user-label"
							class="property-label"><g:message
									code="reservation.user.label" default="User" /></span> <span
							class="property-value" aria-labelledby="user-label"><g:link
									controller="user" action="show"
									id="${reservationInstance?.user?.id}">
									${reservationInstance?.user?.encodeAsHTML()}
								</g:link></span></li>
					</g:if>

				</ol>
				<g:form url="[resource:reservationInstance, action:'delete']"
					method="DELETE">
					<fieldset class="buttons">
						<g:if test="${session.rol == 'Admin'}">
							<g:link class="edit" action="edit"
								resource="${reservationInstance}">
								<g:message code="default.button.edit.label" default="Edit" />
							</g:link>
						</g:if>
						<%--					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />--%>
					</fieldset>
				</g:form>
			</div>
		</div>
	</section>
</body>
</html>
