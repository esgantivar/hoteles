<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="hoteleriaLayout">
<g:set var="entityName"
	value="${message(code: 'service.label', default: 'Service')}" />
<title><g:message code="default.create.label"
		args="[entityName]" /></title>
</head>
<body>
	<section id="contact-page">
		<div class="container">
			<div class="center">
				<br /> <br />
				<h2>Crear Servicio</h2>
				<p class="lead">Ingresa los datos del servicio</p>
				<p class="lead" style="color: red">
					<g:hasErrors bean="${serviceInstance}">
						<ul class="errors" role="alert">
							<g:eachError bean="${serviceInstance}" var="error">
								<li
									<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
										error="${error}" /></li>
							</g:eachError>
						</ul>
					</g:hasErrors>
				</p>
			</div>
			<div class="row contact-wrap">
				<div class="status alert alert-success" style="display: none"></div>
				<g:form url="[resource:serviceInstance, action:'save']">
					<fieldset class="form">
						<g:render template="form" />
					</fieldset>
					<fieldset class="buttons">
						<g:submitButton name="create" class="btn btn-primary btn-lg"
							value="${message(code: 'default.button.create.label', default: 'Create')}" />
					</fieldset>
				</g:form>
			</div>
		</div>
	</section>
</body>
</html>
