<%@ page import="hoteleria.Service" %>



<div class="fieldcontain ${hasErrors(bean: serviceInstance, field: 'priceService', 'error')} required">
	<label for="priceService">
		<g:message code="service.priceService.label" default="Price Service" />
		<span class="required-indicator">*</span>
	</label>
	<g:field class="form-control" name="priceService" value="${fieldValue(bean: serviceInstance, field: 'priceService')}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: serviceInstance, field: 'description', 'error')} required">
	<label for="description">
		<g:message code="service.description.label" default="Description" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField class="form-control" name="description" required="" value="${serviceInstance?.description}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: serviceInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="service.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField class="form-control" name="name" required="" value="${serviceInstance?.name}"/>

</div>

<%--<div class="fieldcontain ${hasErrors(bean: serviceInstance, field: 'rooms', 'error')} ">--%>
<%--	<label for="rooms">--%>
<%--		<g:message code="service.rooms.label" default="Rooms" />--%>
<%--		--%>
<%--	</label>--%>
<%--	--%>
<%----%>
<%--</div>--%>

