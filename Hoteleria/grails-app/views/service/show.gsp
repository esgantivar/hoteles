
<%@ page import="hoteleria.Service"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="hoteleriaLayout">
<g:set var="entityName"
	value="${message(code: 'service.label', default: 'Service')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<section id="contact-page">
		<div class="container">

			<div class="left">
				<br /> <br />
				<h2>Datos del Servicio</h2>
				<g:if test="${flash.message}">
					<div class="message" role="status">
						${flash.message}
					</div>
				</g:if>
				<ol class="property-list service">

					<g:if test="${serviceInstance?.priceService}">
						<li class="fieldcontain"><span id="priceService-label"
							class="property-label"><g:message
									code="service.priceService.label" default="Price Service" /></span> <span
							class="property-value" aria-labelledby="priceService-label"><g:fieldValue
									bean="${serviceInstance}" field="priceService" /></span></li>
					</g:if>

					<g:if test="${serviceInstance?.description}">
						<li class="fieldcontain"><span id="description-label"
							class="property-label"><g:message
									code="service.description.label" default="Description" /></span> <span
							class="property-value" aria-labelledby="description-label"><g:fieldValue
									bean="${serviceInstance}" field="description" /></span></li>
					</g:if>

					<g:if test="${serviceInstance?.name}">
						<li class="fieldcontain"><span id="name-label"
							class="property-label"><g:message
									code="service.name.label" default="Name" /></span> <span
							class="property-value" aria-labelledby="name-label"><g:fieldValue
									bean="${serviceInstance}" field="name" /></span></li>
					</g:if>

					<g:if test="${serviceInstance?.rooms}">
						<li class="fieldcontain"><span id="rooms-label"
							class="property-label"><g:message
									code="service.rooms.label" default="Rooms" /></span> <g:each
								in="${serviceInstance.rooms}" var="r">
								<span class="property-value" aria-labelledby="rooms-label"><g:link
										controller="room" action="show" id="${r.id}">
										${r?.encodeAsHTML()}
									</g:link></span>
							</g:each></li>
					</g:if>

				</ol>
				<g:form url="[resource:serviceInstance, action:'delete']"
					method="DELETE">
					<fieldset class="buttons">
						<g:if test="${session.rol == 'Admin'}">
							<g:link class="edit" action="edit" resource="${serviceInstance}">
								<g:message code="default.button.edit.label" default="Edit" />
							</g:link>
						</g:if>
						<%--					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />--%>
					</fieldset>
				</g:form>
			</div>
		</div>
	</section>
</body>
</html>
