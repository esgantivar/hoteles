
<%@ page import="hoteleria.Service" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="hoteleriaLayout">
		<g:set var="entityName" value="${message(code: 'service.label', default: 'Service')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<section id="contact-page">
		<div class="container">
			<div class="center">
				<br /> <br />
				<h2>Servicios</h2>
				<p class="lead">Consulta los Servicios</p>
		<div id="list-service" class="content scaffold-list" role="main">
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<fieldset class="form">
			    	<g:form action="list" method="GET">
			        	<div class="fieldcontain">
			            	<label>Search for Price:</label><input type="number" name= "price"/><input type="number" name= "price1"/>
                            <label>&nbsp;</label><input type="submit" value="Search"/>
			        	</div>
			    	</g:form>
				</fieldset>
			<table border="1" style="text-align: center; width: 1000px">
			<thead>
					<tr>
					
						<g:sortableColumn property="priceService" title="${message(code: 'service.priceService.label', default: 'Price Service')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'service.description.label', default: 'Description')}" />
					
						<g:sortableColumn property="name" title="${message(code: 'service.name.label', default: 'Name')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${serviceInstanceList}" status="i" var="serviceInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${serviceInstance.id}">${fieldValue(bean: serviceInstance, field: "priceService")}</g:link></td>
					
						<td>${fieldValue(bean: serviceInstance, field: "description")}</td>
					
						<td>${fieldValue(bean: serviceInstance, field: "name")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${serviceInstanceCount ?: 0}" />
			</div>
		</div>
		</div>
		</div>
	</section>
	</body>
</html>
