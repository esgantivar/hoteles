
<%@ page import="hoteleria.Hotel"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="hoteleriaLayout">
<g:set var="entityName"
	value="${message(code: 'hotel.label', default: 'Hotel')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<%--		<a href="#show-hotel" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>--%>
	<%--		<div class="nav" role="navigation">--%>
	<%--			<ul>--%>
	<%--				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--%>
	<%--				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>--%>
	<%--				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>--%>
	<%--			</ul>--%>
	<%--		</div>--%>
	<section id="contact-page">
		<div class="container">

			<div class="left">
				<br /> <br />
				<h2>Datos del Hotel</h2>
				<g:if test="${flash.message}">
					<div class="message" role="status">
						${flash.message}
					</div>
				</g:if>
				<ol class="property-list hotel">

					<g:if test="${hotelInstance?.name}">
						<li class="fieldcontain"><span id="name-label"
							class="property-label"><g:message code="hotel.name.label"
									default="Name" /></span> <span class="property-value"
							aria-labelledby="name-label"><g:fieldValue
									bean="${hotelInstance}" field="name" /></span></li>
					</g:if>

					<g:if test="${hotelInstance?.telephone}">
						<li class="fieldcontain"><span id="telephone-label"
							class="property-label"><g:message
									code="hotel.telephone.label" default="Telephone" /></span> <span
							class="property-value" aria-labelledby="telephone-label"><g:fieldValue
									bean="${hotelInstance}" field="telephone" /></span></li>
					</g:if>

					<g:if test="${hotelInstance?.city}">
						<li class="fieldcontain"><span id="city-label"
							class="property-label"><g:message code="hotel.city.label"
									default="City" /></span> <span class="property-value"
							aria-labelledby="city-label"><g:fieldValue
									bean="${hotelInstance}" field="city" /></span></li>
					</g:if>

					<g:if test="${hotelInstance?.country}">
						<li class="fieldcontain"><span id="country-label"
							class="property-label"><g:message
									code="hotel.country.label" default="Country" /></span> <span
							class="property-value" aria-labelledby="country-label"><g:fieldValue
									bean="${hotelInstance}" field="country" /></span></li>
					</g:if>

					<g:if test="${hotelInstance?.address}">
						<li class="fieldcontain"><span id="address-label"
							class="property-label"><g:message
									code="hotel.address.label" default="Address" /></span> <span
							class="property-value" aria-labelledby="address-label"><g:fieldValue
									bean="${hotelInstance}" field="address" /></span></li>
					</g:if>

					<g:if test="${hotelInstance?.clasification}">
						<li class="fieldcontain"><span id="clasification-label"
							class="property-label"><g:message
									code="hotel.clasification.label" default="Clasification" /></span> <span
							class="property-value" aria-labelledby="clasification-label"><g:fieldValue
									bean="${hotelInstance}" field="clasification" /></span></li>
					</g:if>

					<g:if test="${hotelInstance?.email}">
						<li class="fieldcontain"><span id="email-label"
							class="property-label"><g:message code="hotel.email.label"
									default="Email" /></span> <span class="property-value"
							aria-labelledby="email-label"><g:fieldValue
									bean="${hotelInstance}" field="email" /></span></li>
					</g:if>

					<g:if test="${hotelInstance?.webpage}">
						<li class="fieldcontain"><span id="webpage-label"
							class="property-label"><g:message
									code="hotel.webpage.label" default="Webpage" /></span> <span
							class="property-value" aria-labelledby="webpage-label"><g:fieldValue
									bean="${hotelInstance}" field="webpage" /></span></li>
					</g:if>

					<g:if test="${hotelInstance?.condandrest}">
						<li class="fieldcontain"><span id="condandrest-label"
							class="property-label"><g:message
									code="hotel.condandrest.label" default="Condandrest" /></span> <span
							class="property-value" aria-labelledby="condandrest-label"><g:fieldValue
									bean="${hotelInstance}" field="condandrest" /></span></li>
					</g:if>

					<g:if test="${hotelInstance?.rooms}">
						<li class="fieldcontain"><span id="rooms-label"
							class="property-label"><g:message code="hotel.rooms.label"
									default="Rooms" /></span> <g:each in="${hotelInstance.rooms}" var="r">
								<span class="property-value" aria-labelledby="rooms-label"><g:link
										controller="room" action="show" id="${r.id}">
										${r?.encodeAsHTML()}
									</g:link></span>
							</g:each></li>
					</g:if>

				</ol>
				<g:form url="[resource:hotelInstance, action:'delete']"
					method="DELETE">
					<fieldset class="buttons">
						<g:if test="${session.rol == 'Admin'}">
							<g:link class="edit" action="edit" resource="${hotelInstance}">
								<g:message code="default.button.edit.label" default="Edit" />
							</g:link>
						</g:if>
						<%--					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />--%>
					</fieldset>
				</g:form>
			</div>
		</div>
	</section>
</body>
</html>
