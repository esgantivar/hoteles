
<%@ page import="hoteleria.Hotel"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="hoteleriaLayout">
<g:set var="entityName"
	value="${message(code: 'hotel.label', default: 'Hotel')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<section id="contact-page">
		<div class="container">
			<div class="center">
				<br /> <br />
				<h2>Hoteles</h2>
				<p class="lead">Consulta los hoteles</p>
				<div id="list-hotel" class="content scaffold-list" role="main">
					<g:if test="${flash.message}">
						<div class="message" role="status">
							${flash.message}
						</div>
					</g:if>
					<fieldset class="form">
						<g:form action="list" method="GET">
							<div class="fieldcontain">
								<label>Search for Hotel:</label><input type="text" name="query" />
								<label>Search for City:</label><input type="text"
									name="querycity" /> <label>Search for Clasification:</label><input
									type="number" name="queryclasi" /><input type="number"
									name="queryclasi1" /> <label>&nbsp;</label><input type="submit"
									value="Search" />
							</div>
						</g:form>
					</fieldset>

					<table border="1" style="text-align: center; width: 1000px">
						<thead>
							<tr>

								<g:sortableColumn property="name"
									title="${message(code: 'hotel.name.label', default: 'Name')}" />

								<g:sortableColumn property="telephone"
									title="${message(code: 'hotel.telephone.label', default: 'Telephone')}" />

								<g:sortableColumn property="city"
									title="${message(code: 'hotel.city.label', default: 'City')}" />

								<g:sortableColumn property="country"
									title="${message(code: 'hotel.country.label', default: 'Country')}" />

								<g:sortableColumn property="address"
									title="${message(code: 'hotel.address.label', default: 'Address')}" />

								<g:sortableColumn property="clasification"
									title="${message(code: 'hotel.clasification.label', default: 'Clasification')}" />

							</tr>
						</thead>
						<tbody>
							<g:each in="${hotelInstanceList}" status="i" var="hotelInstance">
								<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

									<td><g:link action="show" id="${hotelInstance.id}">
											${fieldValue(bean: hotelInstance, field: "name")}
										</g:link></td>

									<td>
										${fieldValue(bean: hotelInstance, field: "telephone")}
									</td>

									<td>
										${fieldValue(bean: hotelInstance, field: "city")}
									</td>

									<td>
										${fieldValue(bean: hotelInstance, field: "country")}
									</td>

									<td>
										${fieldValue(bean: hotelInstance, field: "address")}
									</td>

									<td>
										${fieldValue(bean: hotelInstance, field: "clasification")}
									</td>

								</tr>
							</g:each>
						</tbody>
					</table>
					<div class="pagination">
						<g:paginate total="${hotelInstanceCount ?: 0}" />
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>
