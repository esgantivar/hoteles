<%@ page import="hoteleria.Hotel" %>



<div class="fieldcontain ${hasErrors(bean: hotelInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="hotel.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField class="form-control" name="name" maxlength="50" required="" value="${hotelInstance?.name}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: hotelInstance, field: 'telephone', 'error')} required">
	<label for="telephone">
		<g:message code="hotel.telephone.label" default="Telephone" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField class="form-control" name="telephone" maxlength="15" pattern="${hotelInstance.constraints.telephone.matches}" required="" value="${hotelInstance?.telephone}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: hotelInstance, field: 'city', 'error')} required">
	<label for="city">
		<g:message code="hotel.city.label" default="City" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField class="form-control" name="city" required="" value="${hotelInstance?.city}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: hotelInstance, field: 'country', 'error')} required">
	<label for="country">
		<g:message code="hotel.country.label" default="Country" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField class="form-control" name="country" required="" value="${hotelInstance?.country}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: hotelInstance, field: 'address', 'error')} required">
	<label for="address">
		<g:message code="hotel.address.label" default="Address" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField class="form-control" name="address" required="" value="${hotelInstance?.address}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: hotelInstance, field: 'clasification', 'error')} required">
	<label for="clasification">
		<g:message code="hotel.clasification.label" default="Clasification" />
		<span class="required-indicator">*</span>
	</label>
	<g:select class="form-control" name="clasification" from="${1..5}" class="range" required="" value="${fieldValue(bean: hotelInstance, field: 'clasification')}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: hotelInstance, field: 'email', 'error')} required">
	<label for="email">
		<g:message code="hotel.email.label" default="Email" />
		<span class="required-indicator">*</span>
	</label>
	<g:field class="form-control" type="email" name="email" required="" value="${hotelInstance?.email}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: hotelInstance, field: 'webpage', 'error')} required">
	<label for="webpage">
		<g:message code="hotel.webpage.label" default="Webpage" />
		<span class="required-indicator">*</span>
	</label>
	<g:field class="form-control" type="url" name="webpage" required="" value="${hotelInstance?.webpage}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: hotelInstance, field: 'condandrest', 'error')} required">
	<label for="condandrest">
		<g:message code="hotel.condandrest.label" default="Condandrest" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField class="form-control" name="condandrest" required="" value="${hotelInstance?.condandrest}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: hotelInstance, field: 'rooms', 'error')} ">
	<label for="rooms">
		<g:message code="hotel.rooms.label" default="Rooms" />
		
	</label>
	<g:select class="form-control" name="rooms" from="${hoteleria.Room.list()}" multiple="multiple" optionKey="id" size="5" value="${hotelInstance?.rooms*.id}" class="many-to-many"/>

</div>

