<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="hoteleriaLayout">
<g:set var="entityName"
	value="${message(code: 'hotel.label', default: 'Hotel')}" />
<title><g:message code="default.create.label"
		args="[entityName]" /></title>
</head>
<body>
	<%--		<a href="#create-hotel" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>--%>
	<%--		<div class="nav" role="navigation">--%>
	<%--			<ul>--%>
	<%--				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--%>
	<%--				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>--%>
	<%--			</ul>--%>
	<%--		</div>--%>

	<section id="contact-page">
		<div class="container">
			<div class="center">
				<br /> <br />
				<h2>Crear Hotel</h2>
				<p class="lead">Ingresa los datos del hotel</p>
				<p class="lead" style="color: red">
					<g:hasErrors bean="${hotelInstance}">
							<g:eachError bean="${hotelInstance}" var="error">
								<li style="color: red"
									<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
										error="${error}" /></li>
							</g:eachError>
					</g:hasErrors>

				</p>
			</div>
			<div class="row contact-wrap">
				<div class="status alert alert-success" style="display: none"></div>
				<g:form url="[resource:hotelInstance, action:'save']">
					<div class="col-sm-5 col-sm-offset-3">
						<g:render template="form" />
						<div class="form-group">
							<g:submitButton class="btn btn-primary btn-lg" name="create"
								value="${message(code: 'default.button.create.label', default: 'Registrar')}" />
						</div>
					</div>
				</g:form>
			</div>

		</div>
	</section>
</body>
</html>
