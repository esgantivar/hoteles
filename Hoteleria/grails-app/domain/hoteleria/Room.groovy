package hoteleria

class Room {
	String idRoom
	Integer numerOfPeople
	float priceRoom
	String descriptionRoom
	Date startDateRoom
	Date endDateRoom

	static hasMany = [services:Service, reservations:Reservation]
	static belongsTo = [Reservation]
	
	static constraints = {
		id unique: true
		numerOfPeople min:1
		priceRoom min:1F
		startDateRoom min:new Date()
		//endDateRoom min:'startDateRoom'
		endDateRoom(validator: { val, obj ->
			val?.after(obj.startDateRoom)
		})
	}
	
	
	String toString(){
		"$idRoom"//"$role"+
	}
}
