package hoteleria

class Hotel {	
	Integer id
	String name
	String telephone
	String city
	String country
	String address
	Integer clasification
	String email
	String webpage
	String condandrest
	
	static hasMany = [rooms:Room]
//	static hasMany = [rooms:Room, reserves: Reservation]
	static belongsTo = [Room]
	
	static constraints = {
		id unique:true
		name blank:false, size: 5.value..50
		telephone size: 7..15, matches: "[0-9|-]*"
		city blank:false
		country blank:false
		address blank:false
		clasification range:1..5
		email email:true
		webpage url:true 
	}
	
	
	String toString(){
		"$name"//"$role"+
	}
}
