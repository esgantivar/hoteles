package hoteleria

class User {
	String userName
	String password
	String email
	String address
	String telephone
	String rol

	static hasMany = [reserves:Reservation]
	static belongsTo = [reserves:Reservation]

	static constraints = {
		userName blank: false, size: 4..20
		password blank: false, password: true, size: 4..20
		email unique: true, blank: false, email:true
		telephone size: 7..15, matches: "[0-9|-]*"
		rol nullable:true
		//role inList:['Admin', 'User', 'Manager']
	}

	String toString(){
		"$userName"//"$role"+
	}
}