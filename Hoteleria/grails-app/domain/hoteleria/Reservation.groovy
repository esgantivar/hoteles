package hoteleria

class Reservation {

	Integer idReserv
	float totalPriceReserv
	String pay
	Date emisionDateReserv
	Date invalidDateReserv
	Hotel hotel
	User user
	
	static hasMany=[rooms: Room]

	static constraints = {
		idReserv unique:true
		emisionDateReserv min:new Date()
		//		invalidDateReserv min: emisionDateReserv
		invalidDateReserv(validator: { val, obj ->
			val?.after(obj.emisionDateReserv)
		})
		totalPriceReserv min:0F
		pay inList:["Efecty", "Credit Card", "Debit Card", "Checks"]
	}
	
}
